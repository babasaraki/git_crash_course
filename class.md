### classes[6] = "Crash course on git"

#### Perspetivas em Bioinformática 2021-2022

![Logo EST](assets/logo-ESTB.png)

Francisco Pina Martins

[@FPinaMartins](https://twitter.com/FPinaMartins)

---

### What is git?

* &shy;<!-- .element: class="fragment" -->Was written by Linus Trovlads

&shy;<!-- .element: class="fragment" -->![Linus Trovalds](assets/linus.jpg)

* &shy;<!-- .element: class="fragment" -->Git is a *version control system* (VCS)
  * &shy;<!-- .element: class="fragment" -->What changed?
  * &shy;<!-- .element: class="fragment" -->Who changed it?
  * &shy;<!-- .element: class="fragment" -->When was it changed?
  * &shy;<!-- .element: class="fragment" -->Why was it changed?

|||

### What is git?

* &shy;<!-- .element: class="fragment" -->*Decentralized* VCS
  * &shy;<!-- .element: class="fragment" -->Does not depend on central infrastructure
  * &shy;<!-- .element: class="fragment" -->Ideal for asynchronous collaboration
  * &shy;<!-- .element: class="fragment" -->Each developer has a full copy of the project

---

### Projects in git

* &shy;<!-- .element: class="fragment" -->Are called *Repositories*
  * &shy;<!-- .element: class="fragment" -->Contains all files/dirs
  * &shy;<!-- .element: class="fragment" -->Including the respective edit history
    * &shy;<!-- .element: class="fragment" -->Each change is called a *commit*
      * &shy;<!-- .element: class="fragment" -->Linked
      * &shy;<!-- .element: class="fragment" -->Organized in a tree like fashion as *branches*
      
&shy;<!-- .element: class="fragment" -->![Git branches](assets/git-branches.png)

---

### Online git platforms

![Social coding](assets/social-coding.png)

* &shy;<!-- .element: class="fragment" -->A public place for code hosting
* &shy;<!-- .element: class="fragment" -->Social coding!

---

### Install git

```bash
# Ubuntu (Debian based)
sudo apt update
sudo apt install git
```

```bash
# Manjaro (Arch Linux based)
sudo pacman -Syy
sudo pacman -S git
```

```bash
# Fedora (Red Hat based)
sudo dnf distro-sync
sudo dnf install git
```

---

### Getting (literally) started with git

```bash
mkdir my_new_repo
cd my_new_repo

git init
```

* &shy;<!-- .element: class="fragment" -->This will initiate a new git repository
* &shy;<!-- .element: class="fragment" -->Check out `ls -la` to see what it did

|||

### What if the repo already exists?

```bash
git clone https://gitlab.com/labbinf2020/scripts.git

cd scripts
ls -la
```

* &shy;<!-- .element: class="fragment" -->This will **clone** a pre-existing repository
* &shy;<!-- .element: class="fragment" -->Now `cd` into one of your fresh cloned repositories
* &shy;<!-- .element: class="fragment" -->See how it includes all code and commit history

---

### Ok, so I've written some code. What now?

```bash
cd ~/my_new_repo  # Get back to your own repository
echo "Hello git" > README.md
git add README.md
```

* &shy;<!-- .element: class="fragment" -->This will *stage* the changes for inclusion
* &shy;<!-- .element: class="fragment" -->It can be done multiple times before the next step

---

### Changes staged. Are we there yet?

```bash
git commit -m "Adds a readme file"
```

* &shy;<!-- .element: class="fragment" -->This command will *commit* any staged changes
* &shy;<!-- .element: class="fragment" -->The `-m` switch will add a commit message
  * &shy;<!-- .element: class="fragment" -->These should:
    * &shy;<!-- .element: class="fragment" -->Summarize what the commit does
    * &shy;<!-- .element: class="fragment" -->Be written in the third person
    * &shy;<!-- .element: class="fragment" -->**Not** end with a period

---

### What if I loose track of what I'm doing?

```bash
git status
git log
```

* &shy;<!-- .element: class="fragment" -->`git status` shows:
  * &shy;<!-- .element: class="fragment" -->Any staged changes
  * &shy;<!-- .element: class="fragment" -->Any untracked files
* &shy;<!-- .element: class="fragment" -->`git log` shows:
  * &shy;<!-- .element: class="fragment" -->The commit history

---

### All this is linear! What about those "branches" you promised?

```bash
git branch --list  # Lists all branches
git branch my_new_brunch  # Creates a new branch
git branch -D my_new_brunch  # Whoops! Deletes a typoed branch
git branch my_new_branch  # Now it's correct
# Alternatively:
git branch -m my_new_brunch my_new_branch  

git branch --list  # Look again!
git checkout my_new_branch  # "Enter" your new branch
```

* &shy;<!-- .element: class="fragment" -->`git branch` does a lot of stuff
  * &shy;<!-- .element: class="fragment" -->By default, creating a new branch will start it as a copy of your current branch
  * &shy;<!-- .element: class="fragment" -->You can also use `git branch COMMIT_HASH` to start a new branch from the specified commit

---

### Fine! I'll branch...

```bash
echo "A new feature" > feature.txt
echo "And so long, git!" >> README.md

git diff  # Don't just skim this - understand it!

git add feature.txt README.md
git commit -m "Adds a new feature file and completes the readme"

git checkout master
git merge my_new_branch
```

* &shy;<!-- .element: class="fragment" -->These commands will (in order):
    * &shy;<!-- .element: class="fragment" -->Create `feature.txt` and *alter* `README.md`
    * &shy;<!-- .element: class="fragment" -->Highlight what was just done
    * &shy;<!-- .element: class="fragment" -->Stage and commit the changes
    * &shy;<!-- .element: class="fragment" -->Apply changes from `my_new_branch` to `master`
* &shy;<!-- .element: class="fragment" -->Both branches become even
* &shy;<!-- .element: class="fragment" -->You can now delete `my_new_branch`

---

### So, where do online platforms come into play?

```bash
git remote add <name> URL
git push <name>  # Look, an error!

git push --set-upstream <name> master
# From now on you just need to perform `git push` from this branch!
```

* &shy;<!-- .element: class="fragment" -->These commands will `push` your changes into a remote server
* &shy;<!-- .element: class="fragment" -->Of course, this remote repository needs to be created first
* &shy;<!-- .element: class="fragment" -->Conversely, `git pull` will `pull` changes from a remote repository to your local branch

---

### Now live!

![Now live](assets/now-live-icon.png)

---

### References

* [Official git tutorial](https://git-scm.com/docs/gittutorial)
* [Indexed git reference](https://www.atlassian.com/git/tutorials)
* [Git: The why, what and how in under 30 minutes](https://www.freecodecamp.org/news/learn-the-basics-of-git-in-under-10-minutes-da548267cc91/)
* [Quick and dirty git guide (with cheat-sheet)](https://rogerdudler.github.io/git-guide/)
